'use strict';

const gulp         = require('gulp');
const sass         = require('gulp-sass');
const sourcemaps   = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const sassGlob     = require('gulp-sass-glob');
const stylelint    = require('gulp-stylelint');
const uglify       = require('gulp-uglify');
const browserify   = require('browserify');
const watchify     = require('watchify');
const babel        = require('babelify');
const source       = require('vinyl-source-stream');
const buffer       = require('vinyl-buffer');
const del          = require('del');

//
// JS
//
gulp.task('clean:js', gulp.series(function cleanJs() {
    return del(['./dist/js']);
}));

gulp.task('js', gulp.series(['clean:js'], function compileJs() {
    return bundleJs();
}));

//
// CSS
//
gulp.task('css:clean', gulp.series(function cleanCss() {
    return del(['./dist/css']);
}));

gulp.task('css', gulp.series(['css:clean'], function compileCss() {
  return gulp.src('./assets/scss/theme.scss')
    .pipe(stylelint({
        reporters: [{formatter: 'string', console: true}]
    }))
    .pipe(sassGlob())
    .pipe(sass({
        includePaths: 'node_modules'
    }).on('error', sass.logError))
    .pipe(autoprefixer({
        browsers: ['last 5 versions']
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/css'));
}));

//
// Fonts
//
gulp.task('fonts:clean', gulp.series(function cleanFonts() {
    return del(['./dist/fonts']);
}));

gulp.task('fonts', gulp.series(['fonts:clean'], function copyFonts() {
   return gulp.src('./assets/fonts/**/*').pipe(gulp.dest('./dist/fonts'));
}));

//
// Images
//
gulp.task('img:clean', gulp.series(function cleanImg() {
    return del(['./dist/img']);
}));

gulp.task('img:assets', function copyImgAssets() {
    return gulp.src('./assets/img/**/*').pipe(gulp.dest('./dist/img'));
});

gulp.task('img:favicon', function copyImgFavicon() {
    return gulp.src('./assets/favicon.ico').pipe(gulp.dest('./dist'));
});

gulp.task('img', gulp.series(['img:clean'], ['img:assets', 'img:favicon']));

//
// Task sets
//
gulp.task('watch', gulp.series(function watchFiles() {
    gulp.watch('./assets/scss/**/*.scss', gulp.parallel(['css']));
    bundleJs(true);
    gulp.watch('./assets/img/**/*', gulp.parallel(['img']));
    gulp.watch('./assets/fonts/**/*', gulp.parallel(['fonts']));
}));

gulp.task('default', gulp.series(['css', 'js', 'img', 'fonts']));

//
// Utils
//
function bundleJs(watch) {

    let bundler = browserify('./assets/js/mandelbrot.js', {
        debug: true
    }).transform(babel, {
        presets: ["es2015"]
    });

    if (watch) {
        bundler = watchify(bundler);
        bundler.on('update', function () {
            console.log('Rebundling JS....');
            rebundle();
        });
    }

    function rebundle() {
        let bundle = bundler.bundle()
            .on('error', function (err) {
                console.error(err.message);
                // this.emit('end');
            })
            .pipe(source('mandelbrot.js'))
            .pipe(buffer());

        if (!watch) {
            bundle.pipe(uglify());
        }

        bundle.pipe(sourcemaps.init({loadMaps: true}))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest('./dist/js'));

        return bundle;
    }

    return rebundle();
}
