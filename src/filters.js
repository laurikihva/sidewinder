'use strict';

const _            = require('lodash');
const Path         = require('path');
const beautifyHTML = require('js-beautify').html;
const beautifyJS   = require('js-beautify').js;
const reactDocs    = require('react-docgen-typescript').withDefaultConfig();
const stringify    = require('stringify-object');
const ts           = require('typescript');

const stringifyOptions = {
    indent: '    '
};


module.exports = function(theme, env, app){
    const filePaths = [];
    let tsProgram;

    app.components.load().then(() => {
        app.components.flattenDeep().map((item) => {
            if (!filePaths.includes(item.viewPath)) {
                filePaths.push(item.viewPath);
            }
        });
    });

    function getTsProgram() {
        if (!tsProgram) {
            tsProgram = ts.createProgram(filePaths, {
                jsx: ts.JsxEmit.React,
                module: ts.ModuleKind.CommonJS,
                target: ts.ScriptTarget.Latest
            });
        }

        return tsProgram;
    }

    env.engine.addFilter('url', function(item){
        if (item.isDoc) {
            if (!item.path) {
                return '/';
            }
            return theme.urlFromRoute('page', {path: item.path});
        } else if (item.isComponent || item.isVariant) {
            return theme.urlFromRoute('component', {handle: item.handle});
        } else if (item.isAssetSource) {
            return theme.urlFromRoute('asset-source', {name: item.name});
        } else if (item.isAsset) {
            return Path.join('/', app.get('web.assets.mount'), item.srcPath);
        }
        throw new Error(`Cannot generate URL for ${item}`);
    });

    env.engine.addFilter('beautify', function(str) {
        return beautifyHTML(str, {
            // TODO: move to config
            indent_size: 4,
            preserve_newlines: true,
            max_preserve_newlines: 1
        });
    });

    env.engine.addFilter('beautifyjs', function(str) {
        return beautifyJS(str, {
            // TODO: move to config
            indent_size: 4,
            preserve_newlines: true,
            max_preserve_newlines: 1,
            e4x: true,
        });
    });

    env.engine.addFilter('resourceUrl', function(str) {
        return `/${app.web.get('assets.mount')}/components/${Path.relative(Path.resolve(app.components.get('path')), Path.resolve(str))}`;
    });

    env.engine.addFilter('componentPath', function(str) {
        return Path.relative(process.cwd(), Path.join(app.components.get('path'), Path.relative(Path.resolve(app.components.get('path')), Path.resolve(str))));
    });

    env.engine.addFilter('renderJsx', function(item) {
        const componentName = require(item.viewPath).default.name;

        return item.getPreviewContext().then((context) => {
            let contextString = '<' + componentName;

            for (let key in context) {
                if (context.hasOwnProperty(key) && context[key]) {
                    if (key !== 'children') {
                        let keyValue = '{' + stringify(context[key], stringifyOptions) + '}';

                        if (typeof context[key] === 'string') {
                            keyValue = '"' + context[key] + '"';
                        }

                        contextString += ' ' + key + '=' + keyValue;
                    }
                }
            }

            if (context['children']) {
                contextString += '>'
                contextString += context['children'];
                contextString += '</' + componentName + '>'
            } else {
                contextString += ' />';
            }

            return contextString;
        });
    });

    env.engine.addFilter('getProps', function(str) {
        const parsedFile = reactDocs.parseWithProgramProvider(str, () => {
            return getTsProgram();
        });

        if (parsedFile.length) {
            const props = parsedFile[0].props;

            Object.keys(props).map(function(key, index) {
                if (key === 'children' && props[key].type.name === 'ReactNode') {
                    delete props[key];
                }
            });

            return props;
        }

        return null;
    });

    env.engine.addFilter('fileSize', function formatBytes(bytes, decimals) {
        if(bytes == 0) return '0 Byte';
        let k     = 1000; // or 1024 for binary
        let dm    = decimals + 1 || 3;
        let sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        let i     = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    });

    env.engine.addFilter('linkRefs', function(str, item) {
        if (! (item.isComponent || item.isVariant)) {
            return str;
        }
        const refs = item.references;
        return str.replace(new RegExp(`(${refs.map(r => `\@${r.handle}`).join('|')})`, 'g'), (handle) => {
            try {
                let url = theme.urlFromRoute('component', {
                    handle: handle.replace('@', '')
                });
                const pathify = env.engine.getGlobal('path');
                url = pathify.call(this, url);
                return `<a href="${url}">${handle}</a>`;
            } catch(e) {
                return handle;
            }
        });
    });

 };
