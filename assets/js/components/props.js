'use strict';

const $ = global.jQuery;

class Props {

    constructor(el) {
        this._el      = $(el);
        this._trigger = this._el.find('[data-role="props-trigger"]');

        this._init();
    }

    _init() {
        this._trigger.on('click', (event) => {
            event.preventDefault();

            this._el.toggleClass('is-open');
        });
    }
}

module.exports = Props;
